﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorJuego : MonoBehaviour
{
    public GameObject prefabLata;
    public GameObject prefabSprite;
    public int vidas = 7;
    public int puntos = 0;
    public GameObject textoVidas;
    public GameObject textoPuntos;

    private int puntosAnt; // Puntos del frame anterior

    // Start is called before the first frame update
    void Start()
    {
        //GameObject.Instantiate(prefabLata);

    }

    // Update is called once per frame
    void Update()
    {
        this.textoVidas.GetComponent<UnityEngine.UI.Text>().text = "Vidas: " + this.vidas;
        this.textoPuntos.GetComponent<UnityEngine.UI.Text>().text = "Puntos: " + this.puntos;
    }
    //esto es un nuevo metodo (acion, conjunto de instrucciones, funcion, procedimiento,mensaje)
               
    public void CuandoCapturamosEnemigo()
    {  
        //estamos asignando un nuevo valor a la puntacion,
        //  ue es los puntos actuales + 10 puntos.
        this.puntos = this.puntos + 10;  // this.puntos +=10
        GameObject.Instantiate(prefabLata);
    }

    public void CuandoPerdemosEnemigo()
    { 
        this.vidas = this.vidas - 1; // this.vidas -=1; this.vidas --;
        GameObject.Instantiate(prefabLata);
    }
       
}
