﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class LataProfe : MonoBehaviour
    {
        public float velocidad; // Por defecto, cero
        GameObject jugador;


    //Start se llama la primera vez
    void Start()
    {
        float posInicioX = Random.Range(-10, 10);
        this.transform.position = new Vector3(posInicioX, 10, 0);
        //Buscamos un GameObject por su nombre, sólo hacer en Start()
        this.jugador = GameObject.Find("Jugador_Caballito");
        
    }
    // Update is called once per frame
    
    void Update()
        {
            Vector3 movAbajo = velocidad
                * new Vector3(0, -1, 0)
                * Time.deltaTime;
            this.GetComponent<Transform>().position =
                this.GetComponent<Transform>().position + movAbajo;
            if (this.GetComponent<Transform>().position.y < 0)
            {
                this.GetComponent<Transform>().position = new Vector3(this.transform.position.x, 0, 0);

            if (this.transform.position.x >= jugador.transform.position.x - 3.2f / 2
                && this.transform.position.x <= jugador.transform.position.x + 3.2f / 2)
            {
                GameObject.Find("Controlador_Juego").GetComponent<ControladorJuego>().CuandoCapturamosEnemigo();

                Destroy(this.gameObject);

            } else
            {
                GameObject.Find("Controlador_Juego").GetComponent<ControladorJuego>().CuandoPerdemosEnemigo();
                //GameObject.Find("Controlador_Juego").GetComponent<ControladorJuego>().puntos += 1;

                Destroy(this.gameObject);
            }

            }

    }
}
