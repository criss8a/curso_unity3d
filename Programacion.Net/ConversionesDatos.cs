using System;

public class ConversionesDatos {
	//Convertir numero en texto y float en double
	//Conversiones implicitas
	public static void Main (){
		// Convertir numero en texto:
		int edad = 40;
		string resultado;
		resultado = "" + edad;
		resultado = resultado + " era un numero y ahora es texto";
		Console.WriteLine( resultado );
		
		double numDecimal = 6.4545F;
		resultado = resultado + ", el numDecimal = " + numDecimal;
		Console.WriteLine( resultado );
		
		//Conversiones explicitas, se pone entre parentesis el tipo 
		//a donde se quiere converir:
		float otroDecimal = (float) 1.23456789123;
		resultado = resultado + ", otroDecimal = " + otroDecimal;
		Console.WriteLine( resultado );
		
		//Conversiones complejas: de texto a numero
		int unEntero = Int32.Parse("3434");
		resultado = resultado + ", unEntero = " + unEntero;
		Console.WriteLine( resultado );
		
		//string numA = "15", numB = "7";
		// Haz 	que el programa calcule la suma y muestre el resultado
		
		
		
		
		resultado = "";
		string numA = "15";
		string numB = "7";
		
		int aEntero = Int32.Parse( numA );
		int aEntero = Int32.Parse( numB );
		int resultadoSuma = aEntero + aEntero;
		
		Console.WriteLine( "El resultado es " + resultadoSuma);
		
		
		
	}
}