using System;

public class EstructurasDeControl {

	public static void Main ()
	{
		/*
		EjemploIfSimple();
		EjemploIfComplicado();
		EjercicioIfSumas();
		*/
		EjemploIfConsecutivos();
		
	}
	static void EjemploIfSimple()
	{
		//Condicional simple:  
		//if (condicion, booleano) instruccion; (estructura básica)
		
		if (true) Console.WriteLine("Pues si");
		if (false) Console.WriteLine("Pues va a ser que no");
				
		//Recibe variable
		bool oSioNo = true;
		if (oSioNo) Console.WriteLine("Pues tambien si");
		//o recibimos condicionales
		if (5 == 5) Console.WriteLine("Pues 5 == 5");
		if (4 < 7) Console.WriteLine("Pues esto tampoco se muestra");
			
	}
	static void EjemploIfComplicado()
	{
		//if complicado:
		// if (booleano) instruccionVerdad; else instruccionFalso;
		if (4 >= 7) Console.WriteLine("4 >= 7"); 
		else Console.WriteLine("4 < 7");
		// Podemos separar en varias lineas
		if ("Hola" != "hola") Console.WriteLine("Son dist"); 
		else Console.WriteLine("Son =");
		
	}
	
	static void EjercicioIfSumas()
	{
		//Ejercicio
		//string numA = "20", numB = "30", numC = "40";
		//int resultado = 50;
		
		//Suma las 3 combinaciones (A+B, B+C y A+C) 
		//y que el programa diga cual es igual a resultado.
		//Consola debe mostrar:
		//1ro mostrar los valores: A=20, B=30, C=40, resultado= 50
		//A+B es igual a resultado
		//A+C es distinto a resultado
		//B+C es distinto a resultado
		
		Console.WriteLine("A=20, B=30 y C=40") ;
		
		string numA = "20", numB = "30", numC = "40";
		int resultadoSuma = 50;
		
		int intA, intB, intC;
		intA = Int32.Parse (numA) ;
		intB = Int32.Parse (numB) ;
		intC = Int32.Parse (numC) ;
		
		if(intA+ intB == resultadoSuma)
			Console.WriteLine (" A + B es igual a resultado");
		else
			Console.WriteLine (" A + B es distinto de resultado");
		
		if(intA+ intC == resultadoSuma)
			Console.WriteLine (" A + C es igual a resultado");
		else
			Console.WriteLine (" A + C es distinto de resultado");
		
		if(intB+ intC == resultadoSuma)
			Console.WriteLine (" B + C es igual a resultado");
		else
			Console.WriteLine (" B + C es distinto de resultado");
	}
	static void EjemploIfConsecutivos(){
		Console.WriteLine("Introduzca una opción: ");
		Console.WriteLine(" 1 - Opción primera ");
		Console.WriteLine(" 2 - Opción segunda");
		Console.WriteLine(" 3 - Opción tercera");
		Console.WriteLine(" (*) - Cualquier otra opcion.");
	
	
		ConsoleKeyInfo opcion = Console.ReadKey();
		ConsoleKey conKey = opcion.Key;
		string caracter = conKey.ToString();
		
		Console.WriteLine(" >> " + caracter);
		//Si el caracter es 1 del teclado normal o bien
		//Si el caracter es 1 del teclado numérico
		
		if (caracter == "NumPad1"|| caracter == "D1" )
			Console.WriteLine("Has elegido la primera ");
		else if (caracter == "NumPad2"|| caracter == "D2" )
			Console.WriteLine("Has elegido la segunda");
		else if (caracter == "NumPad3"|| caracter == "D3" )
			Console.WriteLine("Has elegido la tercera");
		else if (caracter == "NumPad4"|| caracter == "D4" )
			Console.WriteLine("Has elegido la cuarta");
		else
			Console.WriteLine (" OPCION NO CONTEMPLADA ");
		

			
	}
		
}