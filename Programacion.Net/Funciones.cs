using System;

public class Funciones {

	public static void Main ()
	{
		// para usar una funcion, ponemos el nombre y entre parentesis los argumentos (param)
		double y ;
		string strValUsu;
		double valUsu;
		Console.WriteLine("Introduzca valor: ");
		strValUsu = Console.ReadLine();
		valUsu = Double.Parse(strValUsu);
		y = FuncionLinal2x3(valUsu);
		Console.WriteLine("Resultado 2*X + 3: " + y);
		
		//Console.WriteLine("Resultado X^2 + 1: " + y);
		
		
		//float valX = (float) valUsu;
		//Console.WriteLine (Resultado X^2 + 1: " + FuncionLinal2x3 (valX));
		
	}
	// Forma de una función estática
	// 	<Modificador acceso> static < tipo dato result> <nombre funcion> (<tipo> parám1, <tipo> parám2...)
	// y luego entre llaves el cuerpo de la función
	//con return podemos devolder un valor
	
	private static double FuncionLinal2x3 (double x)
		
	{
		return x * x + 1;			
	}
	
	private static float FuncionXe12_1(float param) {
		float resultado = param * param + 1;
		return resultado;
	}
	
}