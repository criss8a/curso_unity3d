using System;
/* Estos son los tipos primitivos de C# (igual que en otros lenguajes de prog.)
   
*/
public class TiposDeDatos {
	
    public static void Main ()
	{
		byte unByte = 255; // Nº entero entre 0 y 255,8 bits,
		char unCaracter = 'A'; // Un caracter tb es un num
		
		int numEntero = 1000 ; // Numero entre -2.000.000.000 y +2.000.000.000
		                       //porque ocupa 4 bytes. 2^32 = 4.000.000.000
		Console.WriteLine ("Byte: " + unByte + "char: " + unCaracter);
		Console.WriteLine ("Un char ocupa " + sizeof(char) + " byes ");
		Console.WriteLine ("El entero vale " + numEntero);
		Console.WriteLine ("Y ocupa " + sizeof(int) + " bytes");
		numEntero = 2000000000; // 2 mil millones max
		Console.WriteLine("Ahora el entero vale " + numEntero);
		
		//para guardar numeros mas largos:
		long enteroLargo = 5000000000000L; // 8 bytes
		Console.WriteLine ("Entero largo vale " + enteroLargo);
		
		// tipos de decimales: float (4 bytes) y double (8 bytes) 
		// precisi?n es de: 7-8 cifras. 15-16 cifras en TOTAL
		float numDecimal = 123.456789f;
		Console.WriteLine ("Num decimal float vale " + numDecimal);
		
		//para mas precision: double
		double numDoblePrecision = 12345.67890123456789;
		Console.WriteLine ("Num decimal doble vale " + numDoblePrecision);
		
		// para guardar Si/No, Verdad/Falso, Cero/Uno...
		bool variableBooleana = true;
		Console.WriteLine ("variableBooleana vale " + variableBooleana);
		
		//podemos guardar comparaciones, condiciones, etc...
		variableBooleana = numDecimal > 1000; 
		Console.WriteLine ("variableBooleana ahora es " + variableBooleana);
		variableBooleana = numDecimal <= 1000;
		Console.WriteLine ("variableBooleana ahora es " + variableBooleana);
		
		string cadenaDeTexto = "Pues eso, una cadena de texto";
		Console.WriteLine (cadenaDeTexto);
		Console.WriteLine (cadenaDeTexto + " que " + "permite concatenacion");
		//lo que no permite son conversiones inv?lidas:
		//int otroNumero = 5.234f; no se puede
		string otroTxt = "" + 1;
		
		
	}
	
	
	}